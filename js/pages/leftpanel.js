if(typeof pages == "undefined") pages = [];

pages.leftpanel = async function(context) 
{
		console.log('====>[ leftpanel ]<=====');
		clearPages();

		console.log({'isauth':auth.check(), 'user': auth.user() });

	  	// render menu
	  	await context.render('templates/left-panel.template', {'isauth':auth.check(), 'user': auth.user() }).replace(context.$element());



	  	// remove active state
	    $('#left-panel [href]').removeClass('active');



	    // set active state
	    $('#left-panel [href="'+window.location.hash+'"]').addClass('active');



	    // close menu on click 
	    $('#left-panel').off('click', '[href]');
	   	$('#left-panel').on('click', '[href]', function() {
	   		if(!$(this).attr('href').includes('/logout')) {
	   			setTimeout(()=>new menu().close(),50);
	   		}
	   		console.log('--> click on href');
	   	});



	   	// click on create account
	   	$('#left-panel').off('click', '#register-button');
	   	$('#left-panel').on('click', '#register-button', function() {
	   		
	   		var dialog = new Dialog();


	   		var template = function(){/*
	   			<div class="text-center">Ваш телефон</div>
				<div class="form-group text-left float-label">
                    <input type="tel" id="login-phone" class="form-control text-center" placeholder="+7(111)222-33-44" style="font-size: 30px;">
                    <button class="overlay btn btn-sm btn-link text-success">
                        <i class="fa fa-eye"></i>
                    </button>
                </div>
			*/}.toString().replace('function(){/*','').replace('function (){/*','').replace('*/}','');
	   		
			(async () => {

				if(typeof dialog !== "undefined")
					dialog.remove();

				// создаем диалог
				await dialog.create(template, 200)
					.then( () => {
						dialog.touchDrag(function(e, dx, dy, startLeft, startTop) {
		                    $(this).off('touchend');
		                    if(dy<110)
		                        $(this).on('touchend', () => dialog.animate({'top':'10%'},200));
		                     else
		                        $(this).on('touchend', () => dialog.animate({'top':'100%'},350,true).then( () => dialog.remove() ) );
		                }, null, 'y')
					})
					.then( () => dialog.animate({'top':'10%'},200, true))
					.then( () => dialog.drawButtons('Получить код'))
					.then( () => $('#'+dialog.id).find('input').focus() )
					.then( () => dialog.animate({'top':'12%'},300, true));

				$('#'+dialog.id).find('input').mask('+7(000)000-00-00', {placeholder:"+7(___)___-__-__"});

				// не пускаем дальше 
				// пока не получится 
				// запросить код 
				var coderequested = false;

				var phone;
				var code;

				while(!coderequested)
				{
					await dialog.waitPressButton()
					.then( () => {
							if(dialog.pressed == 'Получить код') {
								$('#'+dialog.id).find('button').attr('disabled',true);
								$('#'+dialog.id).find('input').removeClass('error-animation');
								phone = $('#login-phone').val();
								phone = phone.replace(/[^0-9.]/g, "");
								return auth.requestCode(phone);
							}
						})
					.then( () => { 
							coderequested = true;
						}, () => {
							$('#'+dialog.id).find('button').attr('disabled',false);
							$('#'+dialog.id).find('input').addClass('error-animation');
						});
				}
				
				// подготовим шаблон
				// для ввода кода
				var template2 = function(){/*
		   			<div class="text-center">Код из SMS</div>
					<div class="form-group text-left float-label">
	                    <input type="text" pattern="\d*" id="login-code" class="form-control text-center" placeholder="XXXXXX" style="font-size: 50px;">
	                    <button class="overlay btn btn-sm btn-link text-success">
	                        <i class="fa fa-eye"></i>
	                    </button>
	                </div>
				*/}.toString().replace('function(){/*','').replace('*/}','');

				await dialog.animate({'top':'100%'}, 100, true);
				await dialog.text(template2);
				await dialog.drawButtons('Ок');
				await dialog.animate({'top':'10%'}, 200, true).then( () => $('#'+dialog.id).find('input').focus() );

				var codeSended = false;
				while(!codeSended)
				{
					// console.log('--> wait press button');
					loggedin = await dialog.waitPressButton()
					.then( () => {
							$('#'+dialog.id).find('button').attr('disabled',true);
							$('#'+dialog.id).find('input').removeClass('error-animation');
							code = $('#'+dialog.id).find('input').val();
							return auth.registration(code);
						})
					.then( () => { 
							
							codeSended = true;

							dialog.animate({'top':'100%'},350,true)
								  .then( () => dialog.remove() );
							
							return auth.login(phone,code);

						}, () => {
							$('#'+dialog.id).find('button').attr('disabled',false);
							$('#'+dialog.id).find('input').addClass('error-animation');
						});
				}

				if(!loggedin) return;
				
				context.redirect('#/afterlogin');
				
			})();
			


			

	   	});
}