if(typeof pages == "undefined") pages = [];

pages.ordercar = async function(context) 
{
      

      console.log(context,context);
      var cart            = new Cart();
      var Loading         = new simpleText();

      var cartChangeEvent = new CustomEvent('cartchange', { 'detail': {'cart':cart} });

      // запустим Loading
      (async () => {
                Loading.create('Загружаю карту');
                // Loading.overlay('#00000085', 500, 15000, true, true);                        
                Loading.show(0,38);
          await Loading.delay(1000);
                Loading.move(0,50,100);
      })();

      // запустим отрисовку карты
      promisemap = (async() => {
          map          = new MapDrawer;
          var promise1 = render(context, '#main', 'templates/fullscreen-map.template');
          var promise2 = map.loadScript();
          var promise3 = geo.getGeolocation();


          promise1.catch( reject => {
              Loading.text('Не могу отрендерить страницу :(');
              Loading.move(0, 100, 300);
          });

          promise2.catch( reject => {
              Loading.text('Не могу загрузить скрипт карты :(');
              Loading.move(0, 100, 300);
          });

          promise3.catch( reject => {
              Loading.text('Не могу получить геолокацию :(');
              Loading.move(0, 100, 300);
              Loading.animateOverlay({'opacity':0}, 2500, false)
                     .then( () => Loading.remove() );
          });



          var resren  = await promise1;
          var loaded  = await promise2;
          var geoloc  = await promise3;
          
          console.log('====> geoloc',geoloc);

          var addrprom = geo.getAddress(geoloc);

          await map.init('fulscreen-map', geoloc);
                console.log('must after tilesloading');
                map.drawAimPoint('A');



          addrprom
              .then( address => { 
                  map.drawAddress(address);
                  ( async() =>{
                    cart.setStartpoint(address);
                    cart.render(context); 
                  } )();
                  
                  
                  $('#startpoint').hide();
              })
              .catch( () => {
                  map.hideAddress();
              });

          return Promise.resolve(map);
      })();
      
      // запустим отрисовку нижней панели
      promisebottom = (async () => {
          await panel.render(context, $('#panels1'), 'templates/bottom-panel.template');
          
              // подсказки (поиск адреса)
              geo.getFiasLocation()
                   .then( fiasloc => {
                      placeSearcher = new PlaceSearcher($('[name=startpoint]'), map.getCenter(), fiasloc);
                      
                      placeSearcher.onSearch(async function(resp) {
                          placeSearcher.setCount(7);
                          $('#suggestions').html('');
                          var letter = getLetter($('#way > *').length + 1);
                          
                          var promises = [];
                          $.each(resp.data.list, function(i, suggestion) {
                              promises.push(renderAppend(context, '#suggestions', 'templates/suggestion.template', {'address':suggestion.address, 'lat':suggestion.lat, 'lng':suggestion.lng, 'letter':letter, 'level': suggestion.level}));
                          });
                          return Promise.all(promises);
                      });

                      placeSearcher.onFocus(function() {
                          $('#'+panel.id).stop().animate({'top':'10%'},200);
                          $('#startpoint').slideDown();
                          $('body').scrollTop(0);
                      });
                  });

                panel.touchDrag(function(e, dx, dy, startLeft, startTop) {
                    if(dy<-80) {
                        $(this).off('touchend');
                        $(this).on('touchend', function(){
                            $('#'+panel.id).stop().animate({'top':'10%'},100);
                            $('#startpoint').slideDown();
                        });
                     } else {
                        $(this).off('touchend');
                        $(this).on('touchend', function(){
                          $('#'+panel.id).stop().animate({'top':'80%'},100);
                          $('#startpoint').slideUp();
                        });
                     }
                }, null, 'y');

                // когда карта отрисована
                promisemap.then( map => {
                    map.setMode('change startpoint');
                    map.drawLocationButton()
                       .then( map => map.locationButtonAnimate({'bottom':'20%', 'left':'10px'}, 500) );
                    
                       map.getCenter()
                          .then( center => geo.getAddress(center))
                          .then( address=> panel.toBottom());
                       
                    

                        map.onDragStart(function(m) {
                              $('#'+panel.getId()).animate({  'top':'90%' },100);
                              $('#meAsPassenger').animate({  'top':'40%' },100);
                              $('#meAsPassengerShadow').animate({'opacity':1},100);
                        });

                        map.onDragEnd(function(m) {
                              $('#meAsPassenger').stop().animate({'top':'50%'},50);
                              $('#meAsPassengerShadow').animate({'opacity':0.2},100);
                              panel.toBottom();

                              
                              $('#addressbar>span').html('Ищу...');
                              console.log('map dragend', m);
                              m.getCenter()
                                  .then( center => geo.getAddress(center))
                                  .then(
                                      address => {
                                          if(address == false) {
                                              m.hideAddress();
                                              return;
                                          }
                                          m.drawAddress(address);
                                          console.log('cart', cart);
                                          if(m.mode == 'change startpoint')
                                              cart
                                                .setStartpoint({'address':address.address, 'lat':address.lat, 'lng':address.lng, 'letter':'A'})
                                                .then( cart => cart.render(context) )
                                                .then( () => document.dispatchEvent(cartChangeEvent));
                                              // render(context, '#startpoint', 'templates/startpoint.template', {'address':address.address, 'lat':address.lat, 'lng':address.lng, 'letter':'A'});
                                          if(m.mode == 'change way') {
                                              console.log('last ==>', $('#way [data-suggestion]:last'));
                                              renderReplace(context, $('#way [data-suggestion]:last'), 'templates/suggestion.template', {'address':address.address, 'lat':address.lat, 'lng':address.lng, 'level':'house', 'letter':getLetter($('#way [data-suggestion]').length)});
                                          }

                                          
                                              
                                      }, 
                                      error =>  {
                                          if(error)
                                            m.drawAddressError(error)
                                          else
                                            m.hideAddress();
                                          $('#'+panel.getId()).animate({  'top':'100%' },100);
                                      }
                                  )
                                  .then( res => {
                                      
                                  });
                        });

                        

                        $('#'+panel.id).on('click', '#suggestions [data-suggestion]', function() {
                            var issetCoordinates = $(this).data('lat') && $(this).data('lng') ? true : false;
                            var level = $(this).data('level');
                            var letter = getLetter($('#way [data-suggestion]').length+1);
                            switch(level) 
                            {
                                case 'house':
                                    if(!issetCoordinates) {
                                        $('[name=startpoint]').val($(this).data('address'));
                                        placeSearcher.setCount(1);
                                        placeSearcher.search($(this).data('address'))
                                            .then(()=>{
                                                console.log('after repeat last search workerd');
                                                $('#suggestions [data-suggestion]').first().click();
                                                $('[name=startpoint]').blur();
                                            });
                                        return;
                                    }
                                    var suggestion = { 'address': $(this).data('address'), 'lat':$(this).data('lat'), 'lng': $(this).data('lng'), 'level':$(this).data('level'), 'letter': letter };
                                    console.log('suggestion',suggestion);
                                    panel.toBottom();
                                    new onMapDialog()
                                        .create('Сюда?')
                                        .then( dialog =>  dialog.show(0,38) )
                                        .then( dialog =>  dialog.move(0,'22%',200) )
                                        .then( dialog =>  dialog.addButtons('Да', {'html':'Нет', 'className':'mb-2 btn btn-secondary btn-block', 'data-button':'Нет'}) )
                                        .then( dialog => { 
                                            map.panTo(suggestion.lat, suggestion.lng);
                                            map.drawAimPoint(letter);
                                            map.drawAddress(suggestion);
                                            map.setMode('change way');
                                            cart.add(suggestion);
                                            cart.render(context); 
                                            return dialog.buttonPlaceAnimate({'top':'69%'},200); 
                                        })
                                        .then( dialog => dialog.waitPressButton() )
                                        .then( dialog => { 
                                            if(dialog.pressed == 'Да') {
                                                $('#suggestions').html('');
                                                $('[name=startpoint]').val('');
                                                // $('[name=startpoint]').blur();
                                                map.panTo($('[data-startpoint]').data('lat'), $('[data-startpoint]').data('lng'));
                                                map.drawAimPoint('A');
                                                map.setMode('change startpoint');
                                                $('[name=startpoint]').attr('placeholder', 'Еще куда то?');
                                                $('#'+panel.id).stop().animate({'top':'10%'},100);
                                                map.getCenter()
                                                  .then( center => geo.getAddress(center))
                                                  .then( address => address == false ? map.hideAddress() : map.drawAddress(address) )
                                                  .then( () => document.dispatchEvent(cartChangeEvent));

                                                
                                                
                                                
                                                return dialog.move(0,'-10%',100);
                                            }
                                            if(dialog.pressed == 'Нет') {
                                                $('[name=startpoint]').focus();
                                                map.drawAimPoint('A');
                                                map.setMode('change startpoint');
                                                cart.removeLastSuggestion();
                                                cart.render(context);
                                                document.dispatchEvent(cartChangeEvent);
                                                return dialog.move(0,'-10%',100);
                                            }
                                            // cart.getItems().then( items => {
                                            //         console.log('in cart',items);
                                            //         if(items.length > 1)
                                            //             $('#order-params').slideDown();
                                            //         else 
                                            //             $('#order-params').slideUp();
                                            //     }); 
                                        })
                                        .then( dialog => dialog.remove() );
                                    break;
                                default:
                                    console.log('click to street');
                                    $('[name=startpoint]').val($(this).attr('data-address')+' ');
                                    $('[name=startpoint]').trigger($.Event('keyup', {keyCode: 64}));
                                    $('[name=startpoint]').focus();
                                    break;
                            }
                        });
                        // cancel
                        $('#'+panel.id).on('click', '#way [data-suggestion] .cancel-button', function() {
                            var $suggestion = $(this).parents('[data-suggestion]:eq(0)');
                            var suggestion = { 'address': $suggestion.data('address') };
                            $suggestion.slideUp(250);
                            setTimeout(function(){
                                cart.remove(suggestion);
                                cart.render(context);
                                if($('#way [data-suggestion]').length == 0)
                                    $('[data-startpoint]').attr('placeholder','Куда?');
                                document.dispatchEvent(cartChangeEvent);
                            },250);
                        });
                         
                })
                // если карта не отрисована
                .catch( () => {
                    $('#'+panel.id).stop().animate({'bottom':'-10%'},100);
                });

          return Promise.resolve(panel);
      })();

      Promise.all([promisemap,promisebottom])
          .then(values => {
                    Loading.animate({'top':'-10%'}, 500, false);
                    Loading.animateOverlay({'opacity':0}, 500, false)
                           .then( () => Loading.remove() );
          });

      document.addEventListener('cartchange', function(e){
          console.log('cart change', e.detail);
      });

      /*
      * ===================
      * Когда
      * ===================
      */
      $(document).on('click','[data-order-datetime]',function(e){
          e.preventDefault();
          datepickerDialog();
      });

      var datepicker = null;
      var datedialog = null;
      function datepickerDialog()
      {
          
          (async () => {
            // создаем диалог
            if(datedialog == null) {
              datedialog = new Dialog();
              await datedialog.create('<div id="picker-place"></div>', 200);
            }
            
            datedialog.showOverlay(200);
            // создадим выбор даты и времени
            if(datepicker == null)
            datepicker = new Picker(document.querySelector('input[name=datetime]'), {
              container:'#picker-place',
              controls: true,
              inline: true,
              rows: 3,
              format: 'DD.MM.YYYY HH:mm',
              headers: true,
              increment: {
                year: 1,
                month: 1,
                day: 1,
                hour: 1,
                minute: 10
              },
              months:['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
              monthsShort:['Янв', 'Фев', 'Мар', 'Апр', 'Мая', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
              title: 'Когда?',
              text: {
                title: 'Когда?',
                cancel: 'Отмена',
                confirm: 'Ок',
                year: 'Год',
                month: 'Месяц',
                day: 'День',
                hour: 'Час',
                minute: 'Минута',
                second: 'Секунда',
                millisecond: 'Миллисекунда',
              },
              translate(type, text) {
                  const suffixes = {
                    year: 'год',
                    month: 'месяц',
                    day: ' день',
                    hour: ' часов',
                    minute: ' мин',
                  };
                  switch(type) {
                    case 'month':
                      if('01'==text) return 'Января';
                      if('02'==text) return 'Февраля';
                      if('03'==text) return 'Марта';
                      if('04'==text) return 'Апреля';
                      if('05'==text) return 'Мая';
                      if('06'==text) return 'Июня';
                      if('07'==text) return 'Июля';
                      if('08'==text) return 'Августа';
                      if('09'==text) return 'Сентября';
                      if('10'==text) return 'Октября';
                      if('11'==text) return 'Ноября';
                      if('12'==text) return 'Декабря';
                      break;
                    case 'hour':
                      return text;
                      break;
                    case 'minute':
                      return text;
                      break;
                    case 'day':
                      return text;
                      break;
                  }

                  return Number(text) + suffixes[type];
                }
            });

            await datedialog.animate({'top':'10%'},200, true)
              .then( (datedialog) => datedialog.drawButtons('Ок'))
              .then( (datedialog) => datedialog.animate({'top':'15%'},300, true))
              .then( (datedialog) => datedialog.waitPressButton())
              .then( (datedialog) => {
                  if(datedialog.pressed == 'Ок') {
                      var date = datepicker.getDate(true);
                      $('[data-order-datetime-value]').html(date);
                      $('[data-order-datetime-value]').attr('data-order-datetime-value',date);
                      document.dispatchEvent(cartChangeEvent);
                  }
                  return datedialog;
              })
              .then( (datedialog) => datedialog.animate({'top':'120%'},300, true))
              .then( (datedialog) => datedialog.hideOverlay(200));
            
            

          } ) ();

          
      }
      /*
      * ===================
      * Кол-во мест
      * ===================
      */
      $(document).on('click','[data-seats-selector]',function(e){
          e.preventDefault();
          seatsDialog();
      });

      function seatsText(count) {
          if(count == 1) return 'Поеду один';
          if(count == 2) return 'Два места';
          if(count == 3) return 'Три места';
          if(count == 4) return 'Четыре места';
      }

      var seatsdialog = null
      function seatsDialog() {
          (async () => {
            // создаем диалог
            if(seatsdialog == null) {
              seatsdialog = new Dialog();
              await seatsdialog.create('<div id="seats-select-place"><div class="row"><div class="col-12 text-center">Кол-во мест</div></div><div class="row"><div class="col-4"><i class="material-icons" style="font-size:110px;" data-seats-remove="">remove</i></div><div class="col text-center" style="font-size:110px;margin-top:42px;" data-seats-val="1">1</div><div class="col-4"><i class="material-icons" style="font-size:110px;" data-seats-add="">add</i></div></div></div>', 200);
            }
              seatsdialog.showOverlay(200);
              await seatsdialog.animate({'top':'10%'},200, true)
              .then( (seatsdialog) => seatsdialog.drawButtons('Ок'));

              seatsdialog.waitPressButton().then( seatsdialog => {
                  if(seatsdialog.pressed == 'Ок') {
                      // var date = datepicker.getDate(true);
                      // $('[data-order-datetime-value]').html(date);
                      // $('[data-order-datetime-value]').attr('data-order-datetime-value',date);
                      document.dispatchEvent(cartChangeEvent);
                  }
                  return seatsdialog;
              })
              .then( (seatsdialog) => seatsdialog.animate({'top':'120%'},300, true))
              .then( (seatsdialog) => seatsdialog.hideOverlay(200));



              $(document).off('click', '[data-seats-add]');
              $(document).on('click', '[data-seats-add]', function(){
                  var $el = $('[data-seats-val]');
                  var current = parseInt($el.html());
                  if(current+1 < 5) {
                    $el.html(current+1);
                    $('#seats-select-place').removeClass('error-animation');
                  } else {
                    $('#seats-select-place').addClass('error-animation');
                  }
                  current = parseInt($el.html());
                  $('[data-order-seats-value]').html(seatsText(current));
                  $('[data-order-seats-value]').attr('data-order-seats-value',current);
              });

              $(document).off('click', '[data-seats-remove]');
              $(document).on('click', '[data-seats-remove]', function(){
                  var $el = $('[data-seats-val]');
                  var current = parseInt($el.html());
                  if(current-1 > 0){
                    $el.html(current-1);
                    $('#seats-select-place').removeClass('error-animation');
                  } else {
                    $('#seats-select-place').addClass('error-animation');
                  }
                  current = parseInt($el.html());
                  $('[data-order-seats-value]').html(seatsText(current));
                  $('[data-order-seats-value]').attr('data-order-seats-value',current);
              });
            
          })();
      }


      

      $('input[name=datetime]').off();
      $('input[name=datetime]').on('click',function(e){
          
      });
      
}