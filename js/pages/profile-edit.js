if(typeof pages == "undefined") pages = [];
if(typeof pages.profile == "undefined") pages.profile = [];
// if(typeof pages.profile.edit == "undefined") pages.profile = [];

pages.profile.edit = async function(context) {
	
	clearPages();

	user = await new User().whereId(auth.id()).get();
	console.log(user);

	await render(context, '#main', 'templates/profile-edit.template', user);

	$('#profile-page input[name=phone]').mask('+7(000)000-00-00', {placeholder:"+7(___)___-__-__"});

	function saveProfile() {
		userService = new User();
		userService._user.id = $('#profile-page input[name=id]').val();
		userService._user.name = $('#profile-page input[name=name]').val();
		userService.save();
	}

	$('#profile-page input[name=name]').on('blur', saveProfile);
	$('#profile-page input[name=phone]').on('blur', saveProfile);

	$('#getPhoto').off('click');
	$('#getPhoto').on('click', function()
	{
		function onSuccess(imageURI) {
		    var image = document.getElementById('profile-edit-avatar');
		    image.src = imageURI;
		}

		function onFail(message) {
		    alert('Failed because: ' + message);
		}

		navigator.camera.getPicture(onSuccess, onFail, { 
			quality: 50,
	    	destinationType: Camera.DestinationType.FILE_URI 
	    });
	});
	
}