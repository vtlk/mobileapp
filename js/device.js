function DeviceInfo()
{
	this.info;

	this.readInfo = function() {

		if(typeof device == "undefined") {
			device = {
				cordova : false,
				model : false,
				platform : false,
				uuid : this.getOrNewGeneratedUid(),
				version : false,
				manufacturer : false,
				isVirtual : true,
				serial : false,
			}
		}

		this.info = {
			cordova	 	 : device.cordova,
			model 	  	 : device.model,
			platform 	 : device.platform,
			uid	 	     : device.uuid,
			version	 	 : device.version,
			manufacturer : device.manufacturer,
			isVirtual	 : device.isVirtual ? 1 : 0,
			serial 		 : device.serial,
		}

		if(!this.info.uid) this.info.uid = this.getOrNewGeneratedUid();
		
		return this;
	}

	this.getDeviceInfo = function() {
		return this.info;
	}

	this.getDeviceUid = function() {
		return this.info.uid;
	}

	this.getOrNewSecretPhrase = function(clear) {
		if(typeof clear !== "undefined" && clear == true) {
			store.clear('secret');
		}
		var secret = store.get('secret');
		if(!secret) {
			secret = newuid();
			store.set('secret', secret);
		}
		return secret;
	}

	this.getOrNewGeneratedUid = function() {
		var uid = store.get('uid');
		if(!uid) {
			console.log('device uid generated');
			uid = newuid();
			store.set('uid',uid);
		} else {
			console.log('device uid from store');
		}
		return uid;
	}
	
}