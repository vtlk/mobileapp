
function simpleText()
{
	this.id = newuid();
	this.$element = null;

	this.createPromise;
	this.overlayPromise;
	var _this = this;

	this.overlay = async function(color, speed, delay, skipspeed, skipdelay)
	{
		var wait_ms = 0;
		if(!skipspeed) wait_ms = wait_ms + speed;
		if(!skipdelay) wait_ms = wait_ms + delay;

		this.overlayPromise = new Promise(resolve => setTimeout(resolve, wait_ms+10));

		var div   = document.createElement('div');
		div.id    = this.id+'overlay';
		div.style.width = '100%';
		div.style.height = '100%';
		div.style.background = color;
		div.style.position = 'fixed';
		div.style.top = '0';
		div.style.left = '0';
		div.style.opacity = '0';
		div.style.zIndex = '30';

		body = document.getElementsByTagName('body')[0];
		body.appendChild(div);

		$('#'+this.id+'overlay').animate({'opacity':1}, speed);
		if(!skipspeed)
		await new Promise(resolve => setTimeout(resolve, speed));
		if(!skipdelay)
		await new Promise(resolve => setTimeout(resolve, delay));
		console.log('create overlay');
		
		return this.overlayPromise.then( () => Promise.resolve(_this) );
	}

	this.animateOverlay = async function(o,delay,awaiting)
	{
		$('#'+this.id+'overlay').animate(o, delay);
		
		if(!awaiting)
			await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.animate = async function(o,delay,awaiting)
	{
		$('#'+this.id).animate(o, delay);
		
		if(!awaiting)
			await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.create = async function(text, delay) 
	{
		console.log('Modals.showSimpleText', text, delay);

		var div  = document.createElement('div');
		var span = document.createElement('span');
		
		div.id 			  = this.id;
		div.className 	  = 'notify simple-text';
		div.style.display = 'none';
		div.style.zIndex  = '50';
		span.innerHTML 	  = text;

		div.appendChild(span);

		body = document.getElementsByTagName('body')[0];
		body.appendChild(div);

		this.$element = $('#'+this.id);
		console.log('create simple text');
		this.createPromise = Promise.resolve(this);
		return this.createPromise;
	}

	this.show = async function(x, y)
	{
		this.$element.css({
			'left':x+'%',
			'top':y+'%'
		});
		this.$element.show();
		return Promise.resolve(this);
	}

	this.delay = async function(delay)
	{
		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.css = async function(o)
	{
		$('#'+this.id).find('span').css(o);
		return Promise.resolve(this);
	}

	this.text = function(str)
	{
		$('#'+this.id).find('span').html(str);
		return Promise.resolve(this);
	}

	this.move = async function(x,y, delay)
	{
		$('#'+this.id).animate({
			'left':x,
			'top':y
		}, delay);

		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.remove = async function()
	{
		console.log('remove simple-text');
		await this.createPromise;
		$('#'+this.id).remove();
		console.log('simple-text text removed');
		await this.overlayPromise;
		$('#'+this.id+'overlay').remove();
		console.log('simple-text overlay removed');
		return Promise.resolve(true);
	}

}

function onMapDialog()
{
	this.id = newuid();
	this.$element = null;
	this.buttons = [];
	this.pressed;

	var _this = this;

	this.overlay = async function(color, speed, delay, skipspeed, skipdelay)
	{
		var div   = document.createElement('div');
		div.id    = this.id+'overlay';
		div.style.width = '100%';
		div.style.height = '100%';
		div.style.background = color;
		div.style.position = 'fixed';
		div.style.top = '0';
		div.style.left = '0';
		div.style.opacity = '0';
		div.style.zIndex = '30';

		body = document.getElementsByTagName('body')[0];
		body.appendChild(div);

		$('#'+this.id+'overlay').animate({'opacity':1}, speed);
		if(!skipspeed)
		await new Promise(resolve => setTimeout(resolve, speed));
		if(!skipdelay)
		await new Promise(resolve => setTimeout(resolve, delay));

		return Promise.resolve(this);
	}

	this.create = async function(text, delay)
	{
		console.log('Modals.onMapDialog', text, delay);

		var div  			= document.createElement('div');
		var span 			= document.createElement('span');
		var buttonPlace  	= document.createElement('div');
		
		buttonPlace.id 	  		= this.id+'buttonPlace';
		buttonPlace.className 	= 'buttonPlace';
		div.id 			  		= this.id;
		div.className 	  		= 'notify simple-text';
		div.style.display 		= 'none';
		div.style.zIndex  		= '50';
		span.innerHTML 	  		= text;

		div.appendChild(span);

		wrapper = document.getElementById('wrapper');
		wrapper.appendChild(div);
		wrapper.appendChild(buttonPlace);

		this.$element = $('#'+this.id);

		return Promise.resolve(this);
	}

	this.addButtons = async function()
	{
		this.buttons = [];
		var buttonPlace = document.getElementById(this.id+'buttonPlace');
		for (var i = 0; i < arguments.length; i++) {
			if(typeof arguments[i] == "object" && typeof arguments[i]['data-button'] == "undefined") 
				console.error("у кнопки не указан обязательный атрибут data-button");
			var button = document.createElement('button');
			button.type = customOrDefault(arguments[i], 'type','button');
			button.className = customOrDefault(arguments[i], 'className','mb-2 btn btn-lg btn-default btn-block');
			button.innerHTML = customOrDefault(arguments[i], 'html', arguments[i]);
			button.dataset.button = customOrDefault(arguments[i], 'data-button', arguments[i]);
		    buttonPlace.appendChild(button);
		    this.buttons.push(button);
		}
		return Promise.resolve(this);
	}

	this.waitPressButton = async function()
	{
		promises = [];
		$(this.buttons).each(function(i, b) {
			promises.push(new Promise(resolve => {
					$(b).one('click', function() {
						_this.pressed = $(this).data('button');
						resolve(_this);
					});
				}, reject => {})
			);
		});

		return Promise.race(promises);
	}

	this.buttonPlaceAnimate = async function(params, speed)
	{
		console.log('Modals.buttonPlaceAnimate', params, speed);
		$('#'+this.id+'buttonPlace').animate(params, speed);
		await new Promise(resolve => setTimeout(resolve, speed));
		return Promise.resolve(this);
	}

	this.show = async function(x, y)
	{
		this.$element.css({
			'left':x+'%',
			'top':y+'%'
		});
		this.$element.show();
		return Promise.resolve(this);
	}

	this.delay = async function(delay)
	{
		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.css = async function(o)
	{
		$('#'+this.id).find('span').css(o);
		return Promise.resolve(this);
	}

	this.text = function(str)
	{
		$('#'+this.id).find('span').html(str);
		return Promise.resolve(this);
	}

	this.move = async function(x,y, delay)
	{
		$('#'+this.id).animate({
			'left':x,
			'top':y
		}, delay);

		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.remove = async function()
	{
		$('#'+this.id).remove();
		$('#'+this.id+'overlay').remove();
		$('#'+this.id+'buttonPlace').remove();
		return Promise.resolve(true);
	}

}


function Dialog()
{
	this.id = newuid();
	this.$element = null;
	this.buttons = [];
	this.pressed;

	var _this = this;

	this.overlay = async function(color, speed, delay, waitspeed, waitdelay)
	{
		var div   = document.createElement('div');
		div.id    = this.id+'overlay';
		div.style.width = '100%';
		div.style.height = '100%';
		div.style.background = color;
		div.style.position = 'fixed';
		div.style.top = '0';
		div.style.left = '0';
		div.style.opacity = '0';
		div.style.zIndex = '20';

		body = document.getElementsByTagName('body')[0];
		body.appendChild(div);

		$('#'+this.id+'overlay').animate({'opacity':1}, speed);
		
		if(waitspeed)
			await new Promise(resolve => setTimeout(resolve, speed));
		if(waitdelay)
			await new Promise(resolve => setTimeout(resolve, delay));

		return Promise.resolve(this);
	}

	this.create = async function(text, delay)
	{
		console.log('dialog', text, delay);
		await this.overlay('#00000075',500,0);
		
		var dialog  		= document.createElement('div');
		var dialog_inner  	= document.createElement('div');
		var dialog_buttons  = document.createElement('div');

		dialog.id 	  		= this.id;
		dialog.className 	= 'global-dialog';
		dialog.style.zIndex = '25';
		dialog.style.position = 'fixed';

		dialog_inner.innerHTML 	= text;
		dialog_inner.className  = 'global-dialog-inner';
		dialog_buttons.className = 'global-dialog-buttons';
		dialog_buttons.id = this.id+'buttonPlace';

		dialog.appendChild(dialog_inner);
		dialog.appendChild(dialog_buttons);

		body = document.getElementsByTagName('body')[0];
		body.appendChild(dialog);

		this.$element = $('#'+this.id);

		return Promise.resolve(this);
	}

	this.touchDrag = async function(callback, x, y)
	{
		$('#'+this.id).touchDrag( callback, x, y );
		return Promise.resolve(this);
	}

	this.drawButtons = async function()
	{
		this.buttons = [];
		var buttonPlace = document.getElementById(this.id+'buttonPlace');
		buttonPlace.innerHTML = '';
		for (var i = 0; i < arguments.length; i++) {
			if(typeof arguments[i] == "object" && typeof arguments[i]['data-button'] == "undefined") 
				console.error("у кнопки не указан обязательный атрибут data-button");
			var button = document.createElement('button');
			button.type = customOrDefault(arguments[i], 'type','button');
			button.className = customOrDefault(arguments[i], 'className','mb-2 btn btn-lg btn-default btn-block');
			button.innerHTML = customOrDefault(arguments[i], 'html', arguments[i]);
			button.dataset.button = customOrDefault(arguments[i], 'data-button', arguments[i]);
		    buttonPlace.appendChild(button);
		    this.buttons.push(button);
		}
		return Promise.resolve(this);
	}

	this.waitPressButton = async function()
	{
		promises = [];
		console.log(promises, this.buttons);
		$(this.buttons).each(function(i, b) {
			promises.push(new Promise(resolve => {
					$(b).one('click', function() {
						_this.pressed = $(this).data('button');
						console.log('click', _this.pressed);
						resolve(_this);
					});
				}, reject => {})
			);
		});

		return Promise.race(promises);
	}

	this.animate = async function(params, speed, wait)
	{
		console.log('dialog animate', params, speed);
		$('#'+this.id).stop().animate(params, speed);
		if(wait)
			await new Promise(resolve => setTimeout(resolve, speed));
		return Promise.resolve(this);
	}

	this.show = async function(x, y)
	{
		this.$element.css({
			'left':x+'%',
			'top':y+'%'
		});
		this.$element.show();
		return Promise.resolve(this);
	}

	this.delay = async function(delay)
	{
		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.css = async function(o)
	{
		$('#'+this.id).find('span').css(o);
		return Promise.resolve(this);
	}

	this.call = async function(callback)
	{
		await callback();
		return Promise.resolve(this);
	}

	this.text = function(str)
	{
		$('#'+this.id).find('.global-dialog-inner').html(str);
		return Promise.resolve(this);
	}

	this.move = async function(x,y, delay)
	{
		$('#'+this.id).animate({
			'left':x,
			'top':y
		}, delay);

		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.showOverlay = async function(speed)
	{
		$('#'+this.id+'overlay').animate({opacity:1},speed);
		await new Promise(resolve => setTimeout(resolve, speed));
		$('#'+this.id+'overlay').css({'z-index':'20'});
		return Promise.resolve(this);
	}

	this.hideOverlay = async function(speed) 
	{
		$('#'+this.id+'overlay').animate({opacity:0},speed);
		await new Promise(resolve => setTimeout(resolve, speed));
		$('#'+this.id+'overlay').css({'z-index':'-20'});
		return Promise.resolve(this);
	}

	this.remove = async function()
	{
		$('#'+this.id).remove();
		$('#'+this.id+'overlay').remove();
		return Promise.resolve(true);
	}

}