function PlaceSearcher($input, center, fiaslocation)
{
	console.log('PlaceSearcher:','center',center,'fiaslocation',fiaslocation);
	this.$input = $input;
	this.$suggestionContainer;
	this.suggestionTemplate;
	this.provider 				= new SuggestionsService(center, fiaslocation);
	this.center   				= center;
	this.fiaslocation   		= fiaslocation;
	this.count					= 7;
	this.lastquery 				= '';
	this.lastcallback 			;
	
	var _this     				= this;

	this.create = async function($input, $suggestionContainer, suggestionTemplate)
	{
		this.$input 				= $input;
		this.$suggestionContainer 	= $suggestionContainer;
		this.suggestionTemplate 	= suggestionTemplate;
	}

	this.setCount = async function(count) 
	{
		console.log('set count '+count);
		this.count = count;
		return Promise.resolve(this);
	}

	this.getCount = async function()
	{
		return Promise.resolve(this.count);
	}

	this.onSearch = async function(callback) 
	{
		this.lastcallback = callback;

		this.$input.on('keyup', function() 
		{
			// var count = await _this.getCount();
			this.lastquery = _this.$input.val();

			_this.provider
				.getSuggestions(_this.$input.val(), _this.getCount(), this.fiaslocation)
				.then(response => { 
					console.log(response);
					callback(response);
				});
		});
		return Promise.resolve(this);
	}

	this.search = async function(query)
	{
		console.log('repeat last search, count', _this.getCount(), _this.lastcallback);
		
		return new Promise( (resolve, reject) => {
			_this.provider
				.getSuggestions(query, _this.getCount(), _this.fiaslocation)
					.then(response => { 
						console.log(response);
						resolve(_this.lastcallback(response));
					});
		});
	}

	this.onFocus = async function(callback) 
	{
		this.$input.on('focus', callback);
		return Promise.resolve(this);
	}

	this.onBlur = async function(callback) 
	{
		this.$input.on('blur', callback);
		return Promise.resolve(this);
	}

}