function SuggestionsService(center, fiaslocation)
{

	this.abortController = new AbortController();
	this.center 	     = center;
	this.fiaslocation	 = fiaslocation;
	this.server	 		 = server;

	console.log('SuggestionsService this.center', center);

	this.getSuggestions = async function(string, count, fiaslocation) 
	{
		if(fiaslocation) this.fiaslocation = fiaslocation;
		console.log('get suggestions', string, fiaslocation);

		count = await count;

		var data = {
			input: string
		}

		if(this.center.lat) data.lat = this.center.lat;
		if(this.center.lng) data.lng = this.center.lng;
		if(this.fiaslocation.region_fias_id) data.region_fias_id = this.fiaslocation.region_fias_id;
		if(this.fiaslocation.area_fias_id)   data.area_fias_id = this.fiaslocation.area_fias_id;
		if(this.fiaslocation.city_fias_id)   data.city_fias_id = this.fiaslocation.city_fias_id;
		if(count) data.count = count;

		console.log('abort controller', this.abortController);

		
		// abort old signal
		this.abortController.abort();

		// create new
		this.abortController = new AbortController();

		// set signal to fetch
		var options = { signal: this.abortController.signal };

		// wait 500ms
		await new Promise(resolve => { setTimeout(()=>{ return resolve(true); }, 500) });

		// exit if aborted
		if(this.abortController.signal.aborted) {
			console.log('-----> string ',string, 'aborted');
			return Promise.reject(false);
		}

		// fetch if not aborted
		var json = await this.server.fetch('/maps/suggestionAddressFromText', data, options);
		

		if(!json) 
			return Promise.reject('Не могу подключиться');
		
		console.log('response ok');
		
		return Promise.resolve(json);
	}
}