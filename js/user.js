function User()
{
	this.server = server;
	this.endpoint = '/user';
	this.method = '';
	this.data = {};
	this._user = {};
	
	this._user.id = null;
	this._user.name = null;

	this.whereId = function(id)
	{
		this.method = '/whereId/'+id;
		return this;
	}
	
	this.get = async function()
	{
		if(this.method)
			var json = await this.server.fetch(this.endpoint + this.method, this.data);

		if(!json) 
			return Promise.reject('Не могу подключиться');
		
		console.log('response ok');
		
		return Promise.resolve(json.user);
	}
	
	this.save = async function()
	{
		this.method = '/save/'+this._user.id;
		var json = await this.server.fetch(this.endpoint + this.method, {name:this._user.name});
		console.log('response ok');
		
		return Promise.resolve(json.user);
	}

}
