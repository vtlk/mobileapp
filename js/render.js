
async function render(context, $element, template, data) {
	console.log('start render element ', $element, template, data);
    return new Promise(function(resolve, reject) {
          var r = context.render(template, data);
          console.log('rendering element ', $element, r);
          resolve( r.replace($element) );
    });
}


async function renderReplace(context, $element, template, data) {
	// console.log('start render replace element ', $element, template, data);
    return new Promise(function(resolve, reject) {
    	  (async () => {

    	  	  var r = await context.render(template, data);
	          console.log('rendering element ', $element, r, r.content);
	          resolve( $element.replaceWith(r) );
    	  })()
          
    });
}


async function renderAppend(context, $element, template, data) {
	console.log('start render append element ', $element, template, data);
    return new Promise(function(resolve, reject) {
          var r = context.render(template, data);
          console.log('rendering element ', $element, r);
          resolve( r.appendTo($element) );
    });
}
