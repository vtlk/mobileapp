
function BottomPanel($parent, id)
{
	this.$element = null;
	this.$parent = null;
	this.panel = null;
	this.id = null;

	// construct
	var stick = document.createElement('span');
	var panel = document.createElement('div');
	var inner = document.createElement('div');
	stick.className = 'bottom-stick';
	inner.className = 'bottom-inner';

	panel.appendChild(stick);
	panel.appendChild(inner);
	panel.id = id;

	this.id 	 = id;
	this.panel 	 = panel;
	this.$parent = $parent;
	this.context = null;

	_this = this;

	console.log('panel inited',panel);
	
	this.render = function(context, $element, template, data) 
	{
		this.context = context;
		console.log('start render bottom panel ', $element, template, data);

	    return new Promise(function(resolve, reject) {

	        var r = context.render(template, {
	          	'panel': {'id': _this.id},
	          	'data': data
	        });

			(async ()=>{
				await r.replace($element);
			})().then( () => resolve(_this.$element = $('#'+id)));
	          
	    });
	}

	this.toBottom = async function(callback) 
	{
		console.log('animate bottom panel '+'#'+this.id+' to default position');
		$('#'+this.id).animate({'top':'80%'},{
            duration: 500,
            complete: callback
        });
        await new Promise(resolve => setTimeout(resolve, 500));
        return Promise.resolve(this);
	}

	this.touchDrag = async function(callback, x, y)
	{
		$('#'+this.id).touchDrag( callback, x, y );
		return Promise.resolve(this);
	}

	this.forPassengerLogic = function()
	{
		/**
		* При фокусе на поле "Куда" 
		* открываем панельку
		*/
		var focus = false;
		$('#'+this.id).on('focus', '[name=startpoint]', function(e){
			e.preventDefault();
	        console.log('startpoint focus');
	        $('#'+_this.id).stop().animate({'top':'10%'},50);
	        for (var i = 10; i <= 1000; i=i+30) {
	        	setTimeout(function(){ $('body').scrollTop(0);},i);
	        }
	        if(!focus)
		        setTimeout(function(){
		        	$('[name=startpoint]').focus();
		        	focus = true;
		        },1000);
	        bottomPanelPassenger.displayStartPoint();
	    });

		/**
		* Набор текста куда едем
		* для подсказок
		*/
		// $('#'+this.id).on('keyup', '[name=startpoint]', function(){
		// 	passengerMap.searchPlaces($(this).val())
		// 		.then(response => {
		// 			console.log(response);
		// 			if(response.result) {
		// 				$('#suggestions').html('');
		// 				var count = $('#way [data-suggestion]').length;
		// 				var letter;
		// 				if(count==0) letter = 'Б';
		// 				if(count==1) letter = 'В';
		// 				if(count==2) letter = 'Г';
		// 				if(count==3) letter = 'Д';
		// 				$.each(response.data.list, function(i, suggestion) {
		// 		          	_this.context.render('templates/suggestion.template', {'address':suggestion.address, 'lat':suggestion.lat, 'lng':suggestion.lng, 'letter':letter})
		// 		                 .appendTo($('#suggestions'));
		// 		        });
		// 			} else {
		// 				if(typeof response.data != 'undefined' && typeof response.data.suggestions != 'undefined') {
		// 					if(response.data.suggestions.length==0) {

		// 					}
		// 				}
		// 			}
		// 		});
		// });
		/**
		* Клик по подсказке
		* добавляет точку "в корзину"
		*/
		$('#'+this.id).on('click', '#suggestions [data-suggestion]', function() {
			var address = $(this).attr('data-address');
			var lat = $(this).attr('data-lat');
			var lng = $(this).attr('data-lng');
			var $last = $('#way [data-suggestion]').last();

			// добавлять только если такой уже не добавлен последним
			if(!$last.length || ($last.length && $last.attr('data-address') != address)) {

				_this
					.toBottom()
						.then( bottomPanel => {
							return passengerMap.panTo(lat,lng);
						})
						.then( passengerMap => {

							new onMapDialog()
								.create('Сюда?')
			                      // .then( dialog => { return dialog.overlay('#00000085', 500, 15000, true, true);  })
			                      .then( dialog => { return dialog.show(0,38);  })
			                      .then( dialog => { return dialog.delay(1000); })
			                      .then( dialog => { return dialog.move(0,'22%',100); })
			                      .then( dialog => { return dialog.addButtons('Да', {'html':'Нет', 'className':'mb-2 btn btn-secondary btn-block', 'data-button':'Нет'}); })
			                      .then( dialog => { 
			                      	passengerMap.drawAddress({'address':address,'lat':lat,'lng':lng});
			                      	return dialog.buttonPlaceAnimate({'top':'69%'},200); 
			                      })
			                      .then( dialog => { 
				                      	$('#way').append($(this).clone()).html();
										$('#suggestions').html('');
										$('[name=startpoint]').val('');
										$('[name=startpoint]').blur();
			                      })
			                      ;
								
							
						});
				
			}
		});
		/**
		* Клик по кнопке cancel в подсказке
		* удаляет из корзины
		*/
		$('#'+this.id).on('click', '#way [data-suggestion] .cancel-button', function() {
			var $suggestion = $(this).parents('[data-suggestion]:eq(0)');
				$suggestion
						.animate({'height':'0px'},100,function() {
							$suggestion.remove();
							// если больше подсказок нет
							if($('#way [data-suggestion]').length == 0) {
								$('[name=startpoint]').focus();
							}
						});
		});
	}

	this.startpointClear = async function()
	{
		$('#startpoint').html('');
	}

	this.displayStartPoint = async function()
	{
		render(_this.context, $('#startpoint'), 'templates/startpoint.template', {'address': store.get('startpoint_address'), 'lat':store.get('startpoint_location_lat'), 'lng':store.get('startpoint_location_lat')});
	}

	this.startpointInputBlur = function()
	{
		$('[name=startpoint]').blur();
	}

	this.getId = function() 
	{
		return this.id;
	}

	this.destroy = function()
	{

	}
}