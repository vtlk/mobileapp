function GeoService()
{
	this.lastfiaslocation;
	this.lastaddress;

	this.server = server;

	this.getAddress = async function(coords) 
	{
		json = await this.server.fetch('/maps/coordsToAddress', {lat:coords.lat, lng:coords.lng});
		
		
		if(!json) 
			return Promise.reject('Не могу подключиться :(');
		
		if(json.result == false)
			return Promise.reject(false);


		if(!json.data.address)
			return Promise.reject(false);
		
		// запомним текущее место что бы
		// ограничивать поиск подсказок
		this.lastfiaslocation = {
			region_fias_id: json.data.region_fias_id,
			area_fias_id: json.data.area_fias_id,
			city_fias_id: json.data.city_fias_id
		};

		this.lastaddress = {'address':json.data.address,'lat':coords.lat,'lng':coords.lng};
		return Promise.resolve(this.lastaddress);
	}

	this.getLastAddress = async function()
	{
		return Promise.resolve(this.lastaddress);
	}

	this.getFiasLocation = async function(center)
	{
		if(this.lastfiaslocation)
			return Promise.resolve(this.lastfiaslocation);

		if(typeof center == "object")
			await this.getAddress(center);

		await this.getAddress(await this.getGeolocation());

		return Promise.resolve(this.lastfiaslocation);
	}

	this.getGeolocation = async function () 
	{
	    if (navigator.geolocation) {
	      return new Promise(
	        (resolve, reject) => navigator.geolocation.getCurrentPosition( (geolocation) => resolve({ 
                      'lat': geolocation.coords.latitude,
                      'lng': geolocation.coords.longitude,
                      'acc': geolocation.coords.accuracy
                    }), reject)
	      )
	    } else {
	      return new Promise(
	        resolve => resolve(false)
	      )
	    }
	}

}