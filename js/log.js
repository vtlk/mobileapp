console.log('starting logs');

var original = [];

original.log = console.log;
original.error = console.error;

var _log = function(msg)
{
	if($('#log').length)
		$("#log").append("<div class='log-msg'>" + msg + "</div>");
}

var _err = function(msg)
{
	if($('#log').length)
		$("#log").append("<div class='log-error'>" + msg + "</div>");
}

window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
	msg = 'error: ';
	
	if(errorMsg) 	msg = msg + ((typeof errorMsg == "string") ? errorMsg : JSON.stringify(errorMsg));
	if(url)			msg = msg + ' url: '+((typeof url == "string") ? url : JSON.stringify(url));
	if(lineNumber) 	msg = msg + ' line: '+((typeof lineNumber == "string") ? lineNumber : JSON.stringify(lineNumber));

    _err(msg);
}

window.addEventListener("error", function (e) {
	_err(e.message);
});

console.error = function(msg) {
	_err(msg);
}

// Sammy.addLogger(()=>{ _err(argument[1]) });

// console.log = function(message) {
//     original.log(message);
//     _log(message);
// };