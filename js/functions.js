function loadScript(url, callback)
{
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
};


loadScriptAsync = function(uri){
    return new Promise((resolve, reject) => {
        var tag = document.createElement('script');
        tag.src = uri;
        tag.async = true;
        tag.onload = () => {
          resolve();
        };
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    });
};

function newuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}

function customOrDefault(object, property, defalutValue)
{
    if(typeof object == "object" && typeof object[property] !== "undefined") return object[property];
    return defalutValue;
}

function getLetter(count)
{
    if(count == 0) return 'A';
    if(count == 1) return 'Б';
    if(count == 2) return 'В';
    if(count == 3) return 'Г';
    if(count == 4) return 'Д';
}

function reCountLetters()
{
    $('#way [data-suggestion]').each(function(i,el){
        $(el).find('.letter').html(getLetter(i+1));
    });
}

function getAllEvents(element) 
{
    var result = [];
    for (var key in element) {
        if (key.indexOf('on') === 0) {
            result.push(key.slice(2));
        }
    }
    return result.join(' ');
}

function menu()
{
    this.open = function(){
        $('body, html').addClass('sidemenu-open menuactive');
    }

    this.close = function(){
        $('body, html').removeClass('sidemenu-open');
        setTimeout(function () {
            $('body, html').removeClass('menuactive');
        },1);
    }

    this.toggle = function(){
        if(!this.isopen())
            this.open();
        else
            this.close();
    }

    this.isopen = function() {
        return $('body').hasClass('sidemenu-open') == true;
    }
}

function clearPages()
{
    $('#panels1').html('');
    $('#panels2').html('');
    $('#panels3').html('');
}