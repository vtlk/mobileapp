function ModeSwitcher($parent) 
{
	this.id = 'mode-switcher';
	this.$element = null;
	this.$parent = $parent;
	this.visible = false;
	this.state = 'passenger';
	var _this = this;


	this.firstDraw = function() {
		var mode_switcher = document.createElement('div');
			mode_switcher.id = this.id;
			mode_switcher.style.opacity = 0;
		var img1 = document.createElement('div');
			img1.id = 'img1';
		var img2 = document.createElement('div');
			img2.id = 'img2';
		var color = document.createElement('div');
			color.id = 'color';

			img1.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="450.000000pt" height="168.000000pt" viewBox="0 0 450.000000 168.000000" preserveAspectRatio="xMidYMid meet">'+
				'<mask id="maskLayer">'+
				'<g transform="translate(0.000000,168.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">'+
				'<path d="M3865 1530 c-11 -4 -34 -14 -52 -20 -18 -7 -35 -21 -38 -31 -4 -11 -10 -17 -15 -14 -10 6 -52 -50 -43 -58 3 -4 -1 -17 -9 -29 -27 -42 -4 -188 30 -188 7 0 12 -6 9 -12 -2 -7 10 -21 27 -32 l30 -18 -27 -16 c-15 -9 -27 -20 -27 -24 0 -5 -4 -8 -10 -8 -5 0 -19 -9 -30 -20 -11 -11 -17 -20 -12 -20 4 0 -6 -9 -23 -20 -16 -11 -26 -20 -21 -20 5 0 -3 -11 -18 -25 -14 -13 -26 -29 -26 -35 0 -5 -4 -10 -10 -10 -5 0 -23 -14 -41 -31 -70 -69 -128 -76 -739 -89 -530 -12 -624 -10 -794 16 -89 14 -241 54 -267 69 -5 4 1 15 15 26 29 23 42 70 26 98 -10 19 -20 21 -110 21 -89 0 -100 -2 -113 -21 -17 -23 -19 -21 -37 36 -7 22 -26 68 -41 103 -57 127 -36 122 -526 122 -363 -1 -412 -3 -443 -18 -45 -22 -70 -61 -109 -170 -33 -89 -33 -90 -52 -71 -17 17 -32 19 -116 17 -109 -3 -120 -11 -109 -71 9 -48 45 -67 126 -67 33 0 60 -3 59 -7 0 -5 -29 -30 -64 -58 -101 -80 -99 -74 -103 -339 -2 -126 -1 -239 2 -252 12 -47 30 -54 141 -54 127 0 135 7 135 112 l0 68 535 0 535 0 0 -73 c0 -101 7 -107 137 -107 127 0 133 5 133 112 0 88 23 167 65 223 32 42 98 76 180 93 32 7 308 11 770 13 l720 2 50 -22 c60 -26 99 -62 128 -116 19 -35 22 -58 24 -177 1 -107 5 -143 17 -160 9 -12 16 -19 16 -14 0 5 7 3 15 -4 19 -16 85 -4 85 15 0 7 5 15 11 17 7 2 10 65 9 191 l-1 187 51 0 50 0 -1 -182 c0 -224 -1 -210 11 -203 6 3 10 2 10 -2 0 -5 9 -14 21 -21 25 -16 83 0 96 26 4 9 10 161 13 336 l5 319 42 -41 c23 -23 39 -42 36 -42 -4 0 3 -9 15 -20 74 -69 178 -2 136 87 -9 18 -19 32 -21 29 -6 -5 -53 35 -53 45 0 17 -152 179 -169 179 -6 0 -10 3 -9 8 2 4 0 8 -5 9 -4 2 -18 13 -31 25 -22 22 -22 24 -4 39 10 9 18 24 18 33 0 10 4 15 9 12 12 -8 43 42 35 55 -4 6 -1 15 6 19 11 7 16 59 7 90 -2 8 -7 27 -12 43 -4 15 -11 27 -16 27 -5 0 -9 9 -9 21 0 11 -4 18 -9 14 -5 -3 -12 4 -16 15 -4 11 -10 18 -15 15 -4 -3 -11 2 -14 11 -4 9 -9 14 -12 11 -4 -3 -13 1 -21 9 -8 8 -18 12 -23 9 -5 -3 -11 -1 -15 5 -7 12 -82 12 -110 0z m-2472 -344 c13 -10 127 -273 127 -293 0 -2 -245 -3 -545 -3 -300 0 -545 2 -545 5 0 3 15 45 34 93 18 48 41 109 51 135 30 81 9 77 460 77 322 -1 404 -3 418 -14z m-928 -468 c15 -14 28 -38 31 -59 5 -30 1 -42 -23 -69 -40 -45 -95 -45 -135 -1 -15 17 -28 40 -28 52 0 27 26 76 47 88 31 18 80 12 108 -11z m1140 -3 c34 -35 35 -96 0 -130 -56 -56 -155 -15 -155 64 0 80 99 122 155 66z"/>'+
				'</g>'+
				'</mask>'+
				'<rect id="masked" width="100%" height="100%" fill="#ffffff"></rect>'+
				'</svg>';

		mode_switcher.appendChild(img1);
		// mode_switcher.appendChild(img2);
		img1.appendChild(color);

		$parent.html(mode_switcher.outerHTML);
		this.$element = $('#'+this.id);
	}

	this.show = function() {
		$('#'+this.id).animate({'opacity':'1'},300);
		this.visible = true;
	}

	this.hide = function() {
		$('#'+this.id).animate({'opacity':'0'},300);
		this.visible = false;
	}

	this.isVisible = function() {
		return this.visible;
	}

	this.getState = function() {
		return this.state;
	}

}