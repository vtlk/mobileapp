function server()
{
	this.endpoint = 'https://mobileapp.joomstores.com/api';
	this.device	  = new DeviceInfo();
	this.abort	  = new AbortController();
	this.token	  = null;
	this.tokentype= null;
	this.user 	  = null;
	this.abilities= null;

	this.autologin = async function()
	{
		var json = await this.fetch('/auth/token/autologin', {
			'info': this.device.readInfo().getDeviceInfo(),
			'uid': this.device.readInfo().getDeviceUid(),
			'secret': this.device.getOrNewSecretPhrase()
		});

		if(!json) {
			console.error('autologin token failed');
			return false;
		}

		if(json.token) {
			this.token 		= json.token;
			this.tokentype  = 'basic';
			this.user  		= json.user;
			this.abilities  = json.abilities;
			console.log('autologin token recieved', this.token);
			return true;
		}

		return false;
	}

	this.login = async function(phone, code)
	{
		var json = await this.fetch('/auth/login', {
			'code':code,
			'phone':phone
		});

		if(!json) {
			console.error('login failed');
			return Promise.reject(false);
		}

		if(json.token) {
			this.token 		= json.token;
			this.tokentype  = 'auth';
			this.user  		= json.user;
			this.abilities  = json.abilities;
			console.log('login token recieved', this.token);
			store.set('phone', phone);
			store.set('code', code);
			return true;
		}
		return Promise.reject(false);
	}

	this.logout = async function()
	{
		var json = await this.fetch('/auth/logout', {});

		if(!json)
			return Promise.reject(false);

		if(json.result === true) {
			store.clear('phone');
			store.clear('code');
			this.token 		= null;
			this.tokentype  = null;
			this.user  		= null;
			this.abilities  = [];
			return true;
		}

		return Promise.reject(false);
	}


	this.fetch = async function(method, params, options)
	{
		var headers = {};
		if(!this.token && !method.includes('/token/autologin') && !method.includes('/login')) {
			if(store.has('phone') && store.has('code')) {
				var loggedin = await this.login(store.get('phone'), store.get('code'));
			}

			if(!loggedin)
				loggedin = await this.autologin();
		}
		if(this.token) headers.Authorization = 'Bearer '+this.token;
		headers['Access-Control-Allow-Origin'] = '*';

		if(!options) options = {}
		var defaultOptions = {
		  mode: 'cors',
		  signal: this.abort.signal,
		  headers: headers
		};

		var response = await fetch(this.endpoint + method + '?' + $.param(params), $.extend( defaultOptions, options ));

		if(!response.ok) 
			return Promise.reject(false);

		var json = await response.json();
		if(json.error_message)
			return Promise.reject(json.error_message);

		if(json.errors)
			return Promise.reject(json.errors);
		
		return Promise.resolve(json);
	}

	this.getCredentials = function()
	{
		if(!store.has('phone') || !store.has('code'))
			return false;
		return {
			phone: store.get('phone'),
			code: store.get('code')
		}
	}

}