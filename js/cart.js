function Cart()
{

	this.suggestions = [];
	this.startpoint;

	var _this = this;

	this.setStartpoint = async function(startpoint)
	{
		this.startpoint = startpoint;
		return Promise.resolve(this);
	}

	this.add = async function(suggestion)
	{
		this.suggestions.push(suggestion);
		return Promise.resolve(this);
	}

	this.remove = async function(suggestion)
	{
		console.log('remove', suggestion, 'from', this.suggestions);
		this.suggestions = this.suggestions.filter( el => el.address != suggestion.address );
		console.log(this.suggestions);
		return Promise.resolve(this);
	}

	this.getStartpoint = async function()
	{
		return Promise.resolve(this.startpoint);
	}

	this.getSuggestions = async function()
	{
		return Promise.resolve(this.suggestions);
	}

	this.getItems = async function()
	{
		var items = [];
		items.push(this.startpoint);
		$(this.suggestions).each(function(i, suggestion) {
			items.push(suggestion);
		});
		return Promise.resolve(items);
	}

	this.removeLastSuggestion = async function() {
		this.suggestions.pop();
		return Promise.resolve(this);
	}

	this.render = async function(context)
	{
		render(context, '#startpoint', 'templates/startpoint.template', this.startpoint);
		$('#way').html('');

		var promises = [];
		$(this.suggestions).each(function(i, suggestion) {
			suggestion.letter = getLetter(i + 1);
			promises.push(new Promise((resolve, reject)=>renderAppend(context, '#way', 'templates/suggestion.template', suggestion)));
		});		

	    return Promise.resolve(this);
	}



}