function Auth()
{
	this.server 	= server;
	this.device     = new DeviceInfo;
	this._user		= null;
	this.abilities  = [];
	this.loggedin   = false;

	this.ping = async function()
	{
		console.log('start ping');
		var res = await this.server.fetch('/ping',{});
		this.abilities  = this.server.abilities;
		this._user 		= this.server.user;
		return res;
	}

	/*
	* Запросить код по смс
	* Меняет пароль
	*/
	this.requestCode = async function(phone)
	{
		var json = await this.server.fetch('/auth/requestcode', {
			'phone':phone,
			'uid':this.device.readInfo().getDeviceUid(),
			'secret':this.device.getOrNewSecretPhrase()
		});

		if(json.result == true)
			return Promise.resolve(this);
		return Promise.reject(this);
	}

	this.registration = async function(code)
	{
		var response = await this.server.fetch('/auth/registration', {
			'code':code
		});
		if(response.result == true)
			return true;
		return Promise.reject(false);
	}
	/*
	* Login
	*/
	this.login = async function(phone, code)
	{
		var loggedin = await this.server.login(phone, code);

		if(loggedin) {
			this.abilities = this.server.abilities;
			this._user 	   = this.server.user;
			this.loggedin  = true;
		}
		return loggedin;
	}
	/*
	* Logout
	*/
	this.logout = async function()
	{
		result = await this.server.logout();
		this.device.getOrNewSecretPhrase(true);
		if(result) {
			this.abilities = this.server.abilities;
			this._user 	   = this.server.user;
			this.loggedin  = false;
			return true;
		}
		return Promise.reject(false);
	}
	/*
	* Авторизован ли?
	*/
	this.check = function() 
	{
		if(this.server.tokentype=='auth') {
			this.loggedin = true;
		}
		return this.loggedin;
	}
	/*
	* Get user id
	*/
	this.id = function()
	{
		if(this._user)
			return this._user.id;
		return null;
	}
	/*
	* Get user
	*/
	this.user = function()
	{
		return this._user;
	}

}