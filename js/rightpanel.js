var Rightpanel = function(icon)
{
	this.icon = icon;

	this.create = function(text)
	{
		var body = document.getElementsByTagName('body')[0];
		var div = document.createElement('div');
		var cnt = document.createElement('div');
		var btn = document.createElement('button');

		div.className='filter';
		btn.className='btn btn-default filter-btn shadow';
		btn.innerHTML= '<i class="material-icons">'+this.icon+'</i>';
		cnt.className='container filters-container';
		cnt.innerHTML=text;

		div.appendChild(btn);
		div.appendChild(cnt);
		body.appendChild(div);
	}
}