/*
###################################
Script
###################################
*/

// callback
function initMap()
{
  	console.log('map initialized');
  	return Promise.resolve(1);
};

/*
###################################
Main map class
###################################
*/
function MapDrawer() 
{
	this.google_maps_api_key = 'AIzaSyAgWQdCkDsAoPWxfeYiK3jo92GCNHSklDQ';
	this.element_id 	 = null;
  	this.$map 			 = null;
  	this.map 	  		 = null;
  	this.abortController = new AbortController();
  	this.last_address    = null;
  	this.mode			 = null;
  	this.prev_mode		 = null;

  	var _this = this;

  	this.loadScript = async function()
	{ 
		if(typeof google == 'undefined')
			await loadScriptAsync("https://maps.googleapis.com/maps/api/js?key="+this.google_maps_api_key+"&libraries=places&callback=initMap");
		// await new Promise(resolve => setTimeout(resolve, 5000));
		return Promise.resolve(true);
	}
	

	this.init = async function(element_id, coordinates) 
	{
		this.element_id = element_id;
  		this.$map 		= $('#'+element_id);
  		this.map 	  	= null;

		console.log('create new map '+this.element_id, this.$map.html());

		this.map = new google.maps.Map(document.getElementById(this.element_id), {
	      center: {lat: coordinates.lat, lng: coordinates.lng},
	      zoom: 17,
	      disableDefaultUI: true,
	      gestureHandling: 'greedy',
	    });

		return new Promise( (resolve, reject) => {
			google.maps.event.addListenerOnce(this.map, 'tilesloaded', function(){
		    	console.log('tilesloaded')
		    	return resolve(_this);
		    });
		});
	};

	this.getCenter = async function()
	{
		var center = this.map.getCenter();
		return Promise.resolve({ 'lat': center.lat(), 'lng': center.lng() });
	}

	this.onDragStart = async function(callback)
	{
		this.map.addListener('dragstart', function(){ return callback(_this) });
	}

	this.onDragEnd = async function(callback)
	{
		this.map.addListener('dragend', function(){ return callback(_this) });
	}

	this.drawAddress = async function(address_object) 
	{
		console.log(address_object);
		var timeout = 100;
		console.log('draw address '+address_object.address);
		console.log('addressbar', $('#addressbar'));
		if(!$('#addressbar').length) {
			var addressDiv 		= document.createElement('div');
			var addressSpan 	= document.createElement('span');
			addressDiv.id 		= 'addressbar';
			addressDiv.appendChild(addressSpan);
			this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(addressDiv);
			ms = 500;
		} else {
			ms = 1;
		}
		setTimeout(function() {
			$('#addressbar').animate({'top':'80px'},timeout);
			$('#addressbar>span').css({'background':'#ff0e6c47'});
			$('#addressbar>span').html(address_object.address);
		},ms);
		
		await new Promise(resolve => setTimeout(resolve, timeout+ms));
		return Promise.resolve(this);
	}

	this.rememberAddress = async function(address_object)
	{
		store.set('last_remember_address', address_object);
		return Promise.resolve(this);
	}

	this.getLastRememberedAddress = async function(address_object)
	{
		return Promise.resolve(store.get('last_remember_address'));
	}

	this.drawAddressError = async function(error) 
	{
		if(!error) 
			this.hideAddress(100);
		var timeout = 100;
		console.log('draw address error '+error);
		console.log('addressbar', $('#addressbar'));
		$('#addressbar').animate({'top':'80px'},timeout);
		$('#addressbar>span').css({'background':'#524a4d47'});
		$('#addressbar>span').html(error);
		await new Promise(resolve => setTimeout(resolve, timeout));
		return Promise.resolve(false);
	}

	this.hideAddress = async function(delay)
	{
		$('#addressbar').animate({'top':'-80px'},100);
		await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.getLastAddress = async function() 
	{
		return Promise.resolve(this.last_address);
	}

	this.drawLocationButton = async function() 
	{
		var div 	= document.createElement('div');
		div.id 		= 'tomylocationbutton';
		div.innerHTML = '<i class="material-icons">near_me</i>';
		div.className = 'bg-bluegray shadow-1';

		var _this = this;

		div.addEventListener('click', function() {
			$('#tomylocationbutton').addClass('spin');
			$('#tomylocationbutton').addClass('levitate');
			$('#tomylocationbutton').animate({opacity:0.3});
			$('#meAsPassenger').stop().animate({  'top':'40%' },100);
			$('#meAsPassengerShadow').stop().animate({'opacity':1},100);
		    geo
		    	.getGeolocation()
		    	.then( g => { 
		    		console.log(g, g.coords);
		    		(async () => {
		    			await _this.panTo(g.lat,g.lng);
		    			$('#meAsPassenger').stop().animate({'top':'50%'},50);
                    	$('#meAsPassengerShadow').stop().animate({'opacity':0.2},100);
		    		})();
		    		
		    		$('#tomylocationbutton').removeClass('spin');
		    		$('#tomylocationbutton').removeClass('levitate');
		    		$('#tomylocationbutton').animate({opacity:1});
		    		
		    	});
		});

		this.map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(div);
		
		await this.awaitElement('#tomylocationbutton');
		
		return Promise.resolve(this);
	}

	this.locationButtonAnimate = async function(o, delay, awaiting)
	{
		$('#tomylocationbutton').animate(o, delay);
		
		if(awaiting)
			await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}

	this.panTo = async function(lat,lng)
	{
		await this.map.panTo(new google.maps.LatLng(lat, lng));
		
		return new Promise((resolve,reject)=>{
			google.maps.event.addListenerOnce(_this.map, 'idle', function(){
				resolve(this);
			});
		});
	}

	this.setMode = async function(mode)
	{
		if(this.mode != mode) this.prev_mode = this.mode;
		this.mode = mode;
		console.log('map mode changed ', this.prev_mode, this.mode);
		return Promise.resolve(this);
	}

	this.getPrevMode = async function()
	{
		return Promise.resolve(this.prev_mode);
	}

	this.awaitElement = async function(selector, sec)
	{
		var _this = this;
		return new Promise((resolve,reject)=>{
			var interval = setInterval(function(){
				if($(selector).length) {
					clearInterval(interval);
					resolve(_this);
				}
			},sec);
		});
	}

	/*
	*	AIM POINT
	*/

	this.drawAimPoint = async function(letter)
	{
		var _this = this;

		var passangerDiv 	= document.createElement('div');
		var shadowDiv 		= document.createElement('div');
		var shadowDiv2 		= document.createElement('div');

		if($('#meAsPassenger').length) {
			console.log('aim point exists, set letter', letter)
			$('#meAsPassenger').html(letter);
			return Promise.resolve(this);
		}

		passangerDiv.innerHTML = letter;
		passangerDiv.id 	= 'meAsPassenger';
		shadowDiv.id 		= 'meAsPassengerShadow';

		shadowDiv.appendChild(shadowDiv2);

		
		this.map.controls[google.maps.ControlPosition.CENTER].push(passangerDiv);
		this.map.controls[google.maps.ControlPosition.CENTER].push(shadowDiv);

		var idleListener;
		idleListener = this.map.addListener('idle', () => 
		{
			console.log('idle map');
			google.maps.event.removeListener(idleListener);
			Promise.resolve(this);
		});
	}

	this.animateAimPoint = async function(o, delay, wait)
	{
		$('#meAsPassenger').animate(o, delay);
		
		if(wait)
			await new Promise(resolve => setTimeout(resolve, delay));
		return Promise.resolve(this);
	}
}
