var panel;
var modeswitcher;
var geo;
var server;
var auth;



onDeviceReady = function() {
    console.log('app.js started');


    server                = new server();
    auth                  = new Auth();
    modeswitcher          = new ModeSwitcher($('#mode-switcher-place'));
    modeswitcher.firstDraw();
    panel                 = new BottomPanel($('#wrapper'), 'bottom-passanger');
    geo                   = new GeoService;
    rightpanel            = new Rightpanel('warning');

    rightpanel.create('<div id="log"></div>');

    modeswitcher.show();
    /*
    ###################################
    CONFIGURATION 
    ###################################
    */


    /*
    * ------------------
    * Main screen
    * ------------------
    **/
    var screen = $.sammy('#main', function() {

        this.use('Template');
        
        this.get('#/',                    pages.firstscreen);
        this.get('#/passenger/ordercar',  pages.ordercar);
        this.get('#/passenger/routes',    function(){});
        this.get('#/profile/edit',        pages.profile.edit);
        this.get('#/logout',              pages.logout);
        this.get('#/afterlogin',          ()=>true);
    });

    /*
    * ------------------
    * Left panel
    * ------------------
    **/

    var leftpanel = $.sammy('#left-panel', function() {
      this.use('Template');

      this.get('#/',                   pages.leftpanel);
      this.get('#/passenger/ordercar', pages.leftpanel);
      this.get('#/passenger/routes',   pages.leftpanel);
      this.get('#/profile/edit',       pages.leftpanel);
      this.get('#/logout',             pages.leftpanel);
      this.get('#/afterlogin',         function(){ this.redirect('#/'); });
    });





    

    /*
    ###################################
    RUN
    ###################################
    */

    $(function() {
      $('.footer').css('bottom','-100px');
      auth.ping()
        .then( () => {
                    screen.run('#/');
                    leftpanel.run('#/');
                },
               () => {
                    alert('ping error');
                    console.error('ping error');
               }
            )
        .catch( (e) => {
            // todo: if not connected
            console.error(e.message);
        });
    });

    /*
    ###################################
    events
    ###################################
    */


};



(function($) {
   


        // local
        if(window.location.host.includes('mobileapp')) {
            console.log('app loaded from localhost');
            onDeviceReady();
        }
        // production
        document.addEventListener("deviceready", function(){
            $('<span class="new-notification"></span>').appendTo('.menu-btn');
            onDeviceReady();
        }, false);






        // -----------------------------
        // check and save last app state
        // -----------------------------
        console.log('current hash', window.location.hash);
        
        // $(window).on('hashchange', function() 
        // {
        //     store.set('last_app_hash', window.location.hash);
        //     console.log('last_app_hash saved', store.get('last_app_hash'));
        // });

        // if(window.location.hash != store.get('last_app_hash'))
        // {
        //     window.location.hash = store.get('last_app_hash');
        //     console.log('last_app_hash loaded');
        // }

        if(window.location.hash == '#undefined' || window.location.hash == '#/undefined') window.location.hash = '#/';


    
    
})(jQuery);
